package Temperatura;

public class ConversorTemperatura {
		
		public double celsiusParaFahrenheit(double valorCelsius){
			return 1.8 * valorCelsius + 32;
		}
		
		public double fahrenheitParaCelsius(double valorFahrenheit){
			return (valorFahrenheit - 32)/ 1.8;
		}
	
		public double celsiusParaKelvin (double valorCelsius){
			return valorCelsius + 273;
		}
		
		public double kelvinParaCelsius (double valorKelvin){
			return valorKelvin - 273;
		}
		
		public double fahrenheitParaKelvin (double valorFahrenheit){
			return 5 * (valorFahrenheit - 32)/9 + 273;
		}
	
		public double kelvinParaFahrenheit (double valorKelvin){
			return 1.8 * (valorKelvin - 273) + 32; 
		}
	
}
