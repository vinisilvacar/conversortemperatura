package Temperatura;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConversorTemperaturaTest {
	
	private ConversorTemperatura umConversor;

	@Before	
	public void setUp() throws Exception {
		umConversor = new ConversorTemperatura();
	}
	
	//Teste de Conversão de Celsius Para Fahrenheit
	@Test
	public void testCelsiusParaFahrenheitComValorDouble1() {
		assertEquals(32.0, umConversor.celsiusParaFahrenheit(0.0),0.01);
	}
		
	@Test
	public void testCelsiusParaFahrenheitComValorDouble2() {
		assertEquals(126.86, umConversor.celsiusParaFahrenheit(52.7),0.01);
	}
		
	//Teste de Conversão de Fahrenheit Para Celsius
	@Test
	public void testFahrenheitParaCelsiusComValorDouble1() {
		assertEquals(0.0, umConversor.fahrenheitParaCelsius(32.0), 0.01);
	}
	
	@Test
	public void testFahrenheitParaCelsiusComValorDouble2() {
		assertEquals(35.56, umConversor.fahrenheitParaCelsius(96.0),0.01);
	}
	
	//Teste de Conversão de Celsius Para Kelvin
	@Test
	public void testCelsiusParaKelvinComValorDouble1() {
		assertEquals(umConversor.celsiusParaKelvin(0.0), 273.0, 0.01);
	}
	
	@Test
	public void testCelsiusParaKelvinComValorDouble2() {
		assertEquals(313.0, umConversor.celsiusParaKelvin(40.0), 0.01);
	}
	
	//Teste de Conversão de Kelvin Para Celsius
	@Test
	public void testKelvinParaCelsiusComValorDouble1() {
		assertEquals(27.0, umConversor.kelvinParaCelsius(300.0),0.01);
	}
	
	@Test
	public void testKelvinParaCelsiusComValorDouble2() {
		assertEquals(72.0, umConversor.kelvinParaCelsius(345.0),0.01);
	}
	
	//Teste de Conversão de Fahrenheit Para Kelvin 
	@Test
	public void testFahrenheitParaKelvinComValorDouble1() {
		assertEquals(273.0, umConversor.fahrenheitParaKelvin(32.0),0.01);
	}
	
	@Test
	public void testFahrenheitParaKelvinComValorDouble2() {
		assertEquals(283.0, umConversor.fahrenheitParaKelvin(50.0),0.01);
	}
	
	//Teste de Conversão de Kelvin Para Fahrenheit
	@Test
	public void testKelvinParaFahrenheitComValorDouble1() {
		assertEquals(32.0, umConversor.kelvinParaFahrenheit(273.0),0.01);
	}
		
	@Test
	public void testKelvinParaFahrenheitComValorDouble2() {
		assertEquals(80.6, umConversor.kelvinParaFahrenheit(300.0),0.01);
	}
	/*
	public void test(){
		fail("Not yet implemented");
	}*/

}
